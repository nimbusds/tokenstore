<project name="TokenStore" default="zip-all" basedir=".">

	<!-- Set to current TokenStore version number and version date -->
	<property name="version"      value="0.3"/>
	<property name="version-date" value="2013-01-11"/>
	
	<path id="classpath-compile">
		<fileset dir="lib">
			<include name="*.jar"/>
		</fileset>
		<pathelement location="build/dist"/>
	</path>
	
	<path id="classpath-runtime">
		<fileset dir="lib">
			<include name="*.jar"/>
		</fileset>
		<pathelement location="build/dist"/>
		<pathelement location="build/test"/>
	</path>
	
	<target name="clean">
		<delete dir="procsrc"/>
		<delete dir="build"/>
		<delete dir="javadoc"/>
		<delete dir="jar"/>
		<delete dir="war"/>
		<delete dir="zip"/>
		<delete>
			<fileset dir="." includes="**/TEST*"/>
		</delete>
	</target>
	
	<target name="src">
		<mkdir dir="procsrc/dist"/>
		<copy todir="procsrc/dist">
    			<fileset dir="src"/>
  		</copy>
		
		<!-- Indicate TokenStore version number and version date -->
		<replace dir="procsrc/dist" token="$version$" value="${version}">
			<include name="**/*.java"/>
		</replace>
		<replace dir="procsrc/dist" token="$version-date$" value="${version-date}">
			<include name="**/*.java"/>
		</replace>
	</target>
	
	<target name="javadoc" depends="src">
		<mkdir dir="javadoc"/>
		<javadoc packagenames="com.nimbusds.tokenstore.*"
	        	 sourcepath="procsrc/dist"
			 classpathref="classpath-compile"
			 destdir="javadoc"
			 package="true"
			 linksource="yes"
			 private="false"
			 author="true"
			 version="true"
			 use="true"
			 windowtitle="NimbusDS TokenStore"
			 doctitle="NimbusDS TokenStore">
			<bottom>
				<![CDATA[<i>Copyright &#169; Nimbus Directory Services / 
			        Vladimir Dzhuvinov, 2013. All Rights Reserved.</i>]]>
			</bottom>
		</javadoc>
	</target>
	
	<target name="compile" depends="src">
		<mkdir dir="build"/>
		<mkdir dir="build/dist"/>
		<javac srcdir="procsrc/dist" 
		       destdir="build/dist" 
		       classpathref="classpath-compile" 
		       includeantruntime="false">
			<compilerarg value="-Xlint:deprecation"/>
			<compilerarg value="-Xlint:unchecked"/>
		</javac>
	</target>
	
	<target name="compile-tests">
		<mkdir dir="build/test"/>
		<javac srcdir="test" 
		       destdir="build/test" 
		       classpathref="classpath-compile"
		       includeantruntime="false"/>
	</target>
	
	<target name="compile-all" depends="compile,compile-tests"/>
	
	<target name="junit" depends="compile,compile-tests">
		<property file="test/test.properties"/>
		<junit showoutput="true">
			<classpath refid="classpath-runtime"/>
			<formatter type="plain"/>
			<sysproperty key="test" file="test/test.properties"/>
			<syspropertyset>
				<propertyref prefix="log4j"/>
			</syspropertyset>
			<test name="com.nimbusds.tokenstore.AuthorizationTest"/>
			<test name="com.nimbusds.tokenstore.TokenPairTest"/>
			<test name="com.nimbusds.tokenstore.util.TokenPairGeneratorTest"/>
			<test name="com.nimbusds.tokenstore.infinispan.InfinispanTokenStoreTest"/>
		</junit>
	</target>
	
	<target name="jar" depends="compile">
		<mkdir dir="jar"/>
		<jar destfile="jar/tokenstore-${version}.jar" basedir="build/dist"/>
	</target>
	
	<target name="web-xml-check">
		<xmlvalidate failonerror="yes" 
		             lenient="yes" 
			     warn="yes" 
			     file="conf/web.xml">
			<dtd publicId="-//Sun Microsystems, Inc.//DTD Web Application 2.2//EN" 
			     location="http://java.sun.com/xml/ns/javaee/web-app_2_5.xsd"/>

		</xmlvalidate>
	</target>
	
	<target name="war-debug" depends="jar,web-xml-check">
		<mkdir dir="war/debug"/>
		<war destfile="war/debug/tokenstore.war" webxml="conf/web.xml">
			<webinf file="conf/tokenStore.properties"/>
			<webinf file="conf/log4j.properties"/>
			<lib file="jar/tokenstore-${version}.jar"/>
			<lib dir="lib">
				<exclude name="servlet-api-*.jar"/>
				<exclude name="junit-*.jar"/>
			</lib>
		</war>
	</target>
	
	<target name="war-eval" depends="jar,web-xml-check">
		<mkdir dir="war/eval"/>
		<war destfile="war/eval/tokenstore.war" webxml="conf/web.xml">
			<metainf file="doc/licenses/eval/LICENSE.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-Apache-Log4j.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-CORSFilter.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-JavaMail.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-JsonRpc2-AccessFilter.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-JsonRpc2-Base.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-JsonRpc2-Server.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-Json-Smart.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-PropertyUtil.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-UnboundID-LDAP-SDK.txt"/>
			<webinf file="conf/tokenStore.properties"/>
			<webinf file="conf/log4j.properties"/>
			<lib file="jar/tokenstore-${version}.jar"/>
			<lib dir="lib">
				<exclude name="servlet-api-*.jar"/>
				<exclude name="junit-*.jar"/>
			</lib>
		</war>
	</target>
	
	<target name="war-dist" depends="jar,web-xml-check">
		<mkdir dir="war/dist"/>
		<war destfile="war/dist/tokenstore.war" webxml="conf/web.xml">
			<metainf file="doc/licenses/dist/LICENSE.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-Apache-Log4j.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-CORSFilter.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-JavaMail.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-JsonRpc2-AccessFilter.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-JsonRpc2-Base.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-JsonRpc2-Server.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-Json-Smart.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-PropertyUtil.txt"/>
			<metainf file="doc/licenses/third-party/LICENSE-UnboundID-LDAP-SDK.txt"/>
			<webinf file="conf/tokenStore.properties"/>
			<webinf file="conf/log4j.properties"/>
			<lib file="jar/tokenStore-${version}.jar"/>
			<lib dir="lib">
				<exclude name="servlet-api-*.jar"/>
				<exclude name="junit-*.jar"/>
				<exclude name="yguard-*.jar"/>
			</lib>
		</war>
	</target>
	
	<target name="war-all" depends="war-debug,war-eval,war-dist"/>
	
	<target name="zip-eval" depends="war-eval">
		<mkdir dir="zip/eval/TokenStore-${version}"/>
		<copy file="war/eval/tokenstore.war"       todir="zip/eval/TokenStore-${version}"/>
		<copy file="doc/README.txt"                todir="zip/eval/TokenStore-${version}"/>
		<copy file="doc/CHANGELOG.txt"             todir="zip/eval/TokenStore-${version}"/>
		<copy file="doc/licenses/eval/LICENSE.txt" todir="zip/eval/TokenStore-${version}"/>
		<copy file="shell/jsonrpc2-shell.jar"      todir="zip/eval/TokenStore-${version}"/>
		<zip destfile="zip/eval/TokenStore-eval.zip" 
		     basedir="zip/eval"
		     excludes="**/*.zip"/>
	</target>
	
	<target name="zip-dist" depends="war-dist">
		<mkdir dir="zip/dist/TokenStore-${version}"/>
		<copy file="war/dist/tokenstore.war"    todir="zip/dist/TokenStore-${version}"/>
		<copy file="doc/README.txt"                todir="zip/dist/TokenStore-${version}"/>
		<copy file="doc/CHANGELOG.txt"             todir="zip/dist/TokenStore-${version}"/>
		<copy file="doc/licenses/dist/LICENSE.txt" todir="zip/dist/TokenStore-${version}"/>
		<copy file="shell/jsonrpc2-shell.jar"      todir="zip/dist/TokenStore-${version}"/>
		<zip destfile="zip/dist/TokenStore.zip" 
		     basedir="zip/dist"
		     excludes="**/*.zip"/>
	</target>
	
	<target name="zip-all" depends="zip-eval,zip-dist"/>
	
</project>
