package com.nimbusds.tokenstore.util;


import junit.framework.TestCase;

import com.nimbusds.tokenstore.TokenPair;


/**
 * Tests the token pair generator.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-10)
 */
public class TokenPairGeneratorTest extends TestCase {


	public void testGenPair() {

		TokenPairGenerator gen = new TokenPairGenerator(32);

		assertEquals(32, gen.tokenLength());

		boolean noRefreshToken = false;
		int lifetime = 3600;

		TokenPair pair = gen.generate(noRefreshToken, lifetime);

		System.out.println(pair);

		assertEquals(32, pair.getAccessToken().length());
		assertEquals(32, pair.getRefreshToken().length());

		assertEquals(3600, pair.getLifetime());
	}


	public void testGenPairZeroLifetime() {

		TokenPairGenerator gen = new TokenPairGenerator(32);

		assertEquals(32, gen.tokenLength());

		boolean noRefreshToken = false;
		int lifetime = 0;

		TokenPair pair = gen.generate(noRefreshToken, lifetime);

		System.out.println(pair);

		assertEquals(32, pair.getAccessToken().length());
		assertEquals(32, pair.getRefreshToken().length());

		assertEquals(0, pair.getLifetime());
	}


	public void testGenAccessTokenOnly() {

		TokenPairGenerator gen = new TokenPairGenerator(32);

		assertEquals(32, gen.tokenLength());

		boolean noRefreshToken = true;
		int lifetime = 0;

		TokenPair pair = gen.generate(noRefreshToken, lifetime);

		System.out.println(pair);

		assertEquals(32, pair.getAccessToken().length());
		assertNull(pair.getRefreshToken());

		assertEquals(0, pair.getLifetime());
	}
}