package com.nimbusds.tokenstore.infinispan;


import junit.framework.TestCase;

import org.apache.log4j.PropertyConfigurator;

import com.nimbusds.tokenstore.Authorization;
import com.nimbusds.tokenstore.ReadOnlyAuthorization;
import com.nimbusds.tokenstore.TokenPair;
import com.nimbusds.tokenstore.TokenStore;


/**
 * Tests the Infinispan token store implementation.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-11)
 */
public class InfinispanTokenStoreTest extends TestCase {


	public InfinispanTokenStoreTest(final String name) {

		super(name);

		// Log4j
		PropertyConfigurator.configure(System.getProperties());
	}


	public void testDefaultTokenLength() {

		assertEquals(32, TokenStore.DEFAULT_TOKEN_LENGTH);
	}


	public void testTokenLengthSetter() {

		TokenStore store = new InfinispanTokenStore();

		assertEquals(32, store.getTokenLength());

		store.setTokenLength(16);

		assertEquals(16, store.getTokenLength());
	}


	public void testNonExpiringAccessTokenStore() {

		TokenStore store = new InfinispanTokenStore();

		ReadOnlyAuthorization authz = 
			new Authorization("iss", "sub", "aud", "scope", "data");


		// Add new access token
		TokenPair pair = store.addEntry(authz);

		System.out.println(pair);

		assertEquals(32, pair.getAccessToken().length());
		assertNull(pair.getRefreshToken());
		assertEquals(0, pair.getLifetime());


		// Check access token
		authz = store.getAuthorization(pair.getAccessToken());

		assertNotNull(authz);
		assertEquals("iss", authz.getIssuer());
		assertEquals("sub", authz.getSubject());
		assertEquals("aud", authz.getAudience());
		assertEquals("scope", authz.getScope());


		// Remove access token
		authz = store.removeEntry(pair.getAccessToken());

		assertNotNull(authz);
		assertEquals("iss", authz.getIssuer());
		assertEquals("aud", authz.getAudience());
		assertEquals("scope", authz.getScope());


		// Verify access token has been removed
		assertNull(store.removeEntry(pair.getAccessToken()));
	}


	public void testExpiringAccessTokenStore() {

		TokenStore store = new InfinispanTokenStore();

		ReadOnlyAuthorization authz = 
			new Authorization("iss", "sub", "aud", "scope", "data");


		// Add new access token
		TokenPair pair = store.addEntry(authz, 2);

		System.out.println(pair);

		assertEquals(32, pair.getAccessToken().length());
		assertNull(pair.getRefreshToken());
		assertEquals(2, pair.getLifetime());


		// Check access token
		authz = store.getAuthorization(pair.getAccessToken());

		assertNotNull(authz);
		assertEquals("iss", authz.getIssuer());
		assertEquals("sub", authz.getSubject());
		assertEquals("aud", authz.getAudience());
		assertEquals("scope", authz.getScope());


		// Remove access token
		authz = store.removeEntry(pair.getAccessToken());

		assertNotNull(authz);
		assertEquals("iss", authz.getIssuer());
		assertEquals("sub", authz.getSubject());
		assertEquals("aud", authz.getAudience());
		assertEquals("scope", authz.getScope());


		// Verify access token has been removed
		assertNull(store.removeEntry(pair.getAccessToken()));
	}


	public void testRefreshTokenStore() {

		TokenStore store = new InfinispanTokenStore();

		ReadOnlyAuthorization authz = 
			new Authorization("iss", "sub", "aud", "scope", "data");


		// Add new access token
		TokenPair pair = store.addRefreshableEntry(authz, 5);

		System.out.println(pair);

		assertEquals(32, pair.getAccessToken().length());
		assertEquals(32, pair.getRefreshToken().length());
		assertEquals(5, pair.getLifetime());


		// Check access token
		authz = store.getAuthorization(pair.getAccessToken());

		assertNotNull(authz);
		assertEquals("iss", authz.getIssuer());
		assertEquals("sub", authz.getSubject());
		assertEquals("aud", authz.getAudience());
		assertEquals("scope", authz.getScope());


		// Refresh
		TokenPair newPair = store.refreshEntry(pair.getRefreshToken());
		System.out.println("Refreshed token pair: " + newPair);
		assertNotNull(newPair);
		assertEquals(32, newPair.getAccessToken().length());
		assertEquals(32, newPair.getRefreshToken().length());
		assertEquals(5, newPair.getLifetime());


		// Verify old token is no longer in store
		assertNull(store.getAuthorization(pair.getAccessToken()));


		// Check refreshed access token
		authz = store.getAuthorization(newPair.getAccessToken());

		assertNotNull(authz);
		assertEquals("iss", authz.getIssuer());
		assertEquals("sub", authz.getSubject());
		assertEquals("aud", authz.getAudience());
		assertEquals("scope", authz.getScope());


		// Remove refreshed access token
		authz = store.removeEntry(newPair.getAccessToken());

		assertNotNull(authz);
		assertEquals("iss", authz.getIssuer());
		assertEquals("sub", authz.getSubject());
		assertEquals("aud", authz.getAudience());
		assertEquals("scope", authz.getScope());


		// Verify access token has been removed
		assertNull(store.removeEntry(newPair.getAccessToken()));
	}


	public void testExpiration()
		throws InterruptedException {

		TokenStore store = new InfinispanTokenStore();

		ReadOnlyAuthorization authz = 
			new Authorization("iss", "sub", "aud", "scope", "data");


		// Add new access token
		TokenPair pair = store.addRefreshableEntry(authz, 1);

		System.out.println(pair);

		assertEquals(32, pair.getAccessToken().length());
		assertEquals(32, pair.getRefreshToken().length());
		assertEquals(1, pair.getLifetime());


		// Check access token
		authz = store.getAuthorization(pair.getAccessToken());

		assertNotNull(authz);
		assertEquals("iss", authz.getIssuer());
		assertEquals("sub", authz.getSubject());
		assertEquals("aud", authz.getAudience());
		assertEquals("scope", authz.getScope());


		// Sleep until token expires

		Thread.sleep(3000);


		// Verify expiration
		assertNull(store.refreshEntry(pair.getRefreshToken()));

		assertNull(store.getAuthorization(pair.getAccessToken()));
	}
}