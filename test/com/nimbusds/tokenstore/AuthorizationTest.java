package com.nimbusds.tokenstore;


import junit.framework.TestCase;


/**
 * Tests the authorisation class.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-11)
 */
public class AuthorizationTest extends TestCase {


	public void testConstructorAllDefined() {

		Authorization authz = new Authorization("iss", "sub", "aud", "scope", "data");

		assertEquals("iss", authz.getIssuer());
		assertEquals("sub", authz.getSubject());
		assertEquals("aud", authz.getAudience());
		assertEquals("scope", authz.getScope());
		assertEquals("data", authz.getData());
	}


	public void testConstructorAllNull() {

		Authorization authz = new Authorization(null, null, null, null, null);

		assertNull(authz.getIssuer());
		assertNull(authz.getSubject());
		assertNull(authz.getAudience());
		assertNull(authz.getScope());
		assertNull(authz.getData());
	}
}