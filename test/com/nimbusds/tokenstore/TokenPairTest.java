package com.nimbusds.tokenstore;


import junit.framework.TestCase;


/**
 * Tests the token pair class.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-10)
 */
public class TokenPairTest extends TestCase {


	public void testConstructorAllDefined() {

		TokenPair pair = new TokenPair("abc", "def", 3600);

		assertEquals("abc", pair.getAccessToken());
		assertEquals("def", pair.getRefreshToken());
		assertEquals(3600, pair.getLifetime());

		System.out.println(pair);
	}


	public void testMinConstructorAllDefined() {

		TokenPair pair = new TokenPair("abc", "def");

		assertEquals("abc", pair.getAccessToken());
		assertEquals("def", pair.getRefreshToken());
		assertEquals(0, pair.getLifetime());
	}


	public void testConstructorAccessTokenOnly() {

		TokenPair pair = new TokenPair("abc", null, 3600);

		assertEquals("abc", pair.getAccessToken());
		assertNull(pair.getRefreshToken());
		assertEquals(3600, pair.getLifetime());
	}


	public void testMissingAccessTokenException() {

		try {
			TokenPair pair = new TokenPair(null, null, 3600);

			fail("Failed to raise exception");

		} catch (IllegalArgumentException e) {

			// ok
		}
	}
}