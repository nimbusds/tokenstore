package com.nimbusds.tokenstore.infinispan;


import java.util.concurrent.TimeUnit;

import org.infinispan.Cache;

import org.infinispan.manager.DefaultCacheManager;
import org.infinispan.manager.EmbeddedCacheManager;

import com.nimbusds.tokenstore.ReadOnlyAuthorization;
import com.nimbusds.tokenstore.TokenPair;
import com.nimbusds.tokenstore.TokenStore;

import com.nimbusds.tokenstore.util.TokenPairGenerator;


/**
 * Infinispan - based token store.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-11)
 */
public class InfinispanTokenStore implements TokenStore {


	/**
	 * The Infinispan cache manager.
	 */
	private EmbeddedCacheManager cacheManager;


	/**
	 * Maps access tokens to authorisations.
	 */
	private Cache<String,ExpirableAuthorization> accessTokenMap;


	/**
	 * Maps refresh tokens to access tokens.
	 */
	private Cache<String,String> refreshTokenMap;


	/**
	 * The token pair generator.
	 */
	private TokenPairGenerator tokenPairGenerator;


	/**
	 * Creates a new token store using the default Infinispan cache 
	 * manager.
	 */
	public InfinispanTokenStore() {

		this(new DefaultCacheManager());
	}


	/**
	 * Creates a new token store using the specified Infinispan cache
	 * manager.
	 *
	 * @param cacheManager Infinispan cache manager. Must not be 
	 *                     {@code null}.
	 */
	public InfinispanTokenStore(final EmbeddedCacheManager cacheManager) {

	        if (cacheManager == null)
	                throw new IllegalArgumentException("The cache manager must not be null");

	        this.cacheManager = cacheManager;

	        accessTokenMap = cacheManager.getCache("com.nimbusds.tokenstore.accessTokenMap");

	        refreshTokenMap = cacheManager.getCache("com.nimbusds.tokenstore.refreshTokenMap");

	        tokenPairGenerator = new TokenPairGenerator(DEFAULT_TOKEN_LENGTH);
	}


	@Override
	public int getTokenLength() {

		return tokenPairGenerator.tokenLength();
	}


	@Override
	public void setTokenLength(final int length) {

		TokenPairGenerator tokenPairGenerator = new TokenPairGenerator(length);

		this.tokenPairGenerator = tokenPairGenerator;
	}


	@Override
	public TokenPair addEntry(final ReadOnlyAuthorization authorization) {

		// No refresh token, no expiration
		return addEntry(authorization, true, 0);
	}


	@Override
	public TokenPair addEntry(final ReadOnlyAuthorization authorization, final long lifetime) {

		// No refresh token
		return addEntry(authorization, true, lifetime);
	}


	@Override
	public TokenPair addRefreshableEntry(final ReadOnlyAuthorization authorization, final long lifetime) {

		return addEntry(authorization, false, lifetime);
	}


	/**
	 * Adds a new entry to the token store.
	 *
	 * @param authorization  The authorisation record for the access token.
	 *                       Must not be {@code null}.
	 * @param noRefreshToken If {@code true} no refresh token will be 
	 *                       generated.
	 * @param lifetime       The lifetime of the entry in seconds. Zero 
	 *                       signifies a non-expiring entry.
	 *
	 * @return The generated access and optional refresh token pair. The 
	 *         tokens are set to expire according to the specified 
	 *         lifetime.
	 */
	private TokenPair addEntry(final ReadOnlyAuthorization authorization, 
		                   final boolean noRefreshToken, 
		                   final long lifetime) {


		// Infinispan treats negative lifetimes as no expiration
		long adjustedLifespan = lifetime > 0 ? lifetime : -1l;

		TokenPair pair = null;

		// Guard against token collision
		while (true) {

			pair = tokenPairGenerator.generate(noRefreshToken, lifetime);

			// Put access token
			if (accessTokenMap.putIfAbsent(pair.getAccessToken(), 
				                       new ExpirableAuthorization(authorization, lifetime),
				                       adjustedLifespan,
				                       TimeUnit.SECONDS) == null) {

				// Do we have a refresh token?
				if (pair.getRefreshToken() == null)
					break;

				// Put refresh token
				if (refreshTokenMap.putIfAbsent(pair.getRefreshToken(), 
					                        pair.getAccessToken()) == null) {

					break;
				}

			}
		}
		
		return pair;
	}


	@Override
	public ReadOnlyAuthorization getAuthorization(final String accessToken) {

		return accessTokenMap.get(accessToken);
	}


	@Override
	public ReadOnlyAuthorization removeEntry(final String accessToken) {

		return accessTokenMap.remove(accessToken);
	}


	@Override
	public TokenPair refreshEntry(final String refreshToken) {

		String accessToken = refreshTokenMap.remove(refreshToken);

		// Invalid or expired refresh token?
		if (accessToken == null)
			return null;

		ExpirableAuthorization authorization = accessTokenMap.remove(accessToken);

		// Access token may have also expired
		if (authorization == null)
			return null;

		return addEntry(authorization, false, authorization.getLifetime());
	}
}