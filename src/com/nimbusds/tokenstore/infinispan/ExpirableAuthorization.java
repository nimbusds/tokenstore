package com.nimbusds.tokenstore.infinispan;


import com.nimbusds.tokenstore.Authorization;
import com.nimbusds.tokenstore.ReadOnlyAuthorization;


/**
 * Expirable authorisation.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-11)
 */
class ExpirableAuthorization extends Authorization {


	/**
	 * The lifetime of the entry in seconds. Zero signifies a non-expiring 
	 * entry.
	 */
	private final long lifetime;


	/**
	 * Copy constructor.
	 *
	 * @param authorization The original authorisation. Must not be 
	 *                      {@code null}.
	 * @param lifetime      The lifetime of the entry in seconds. Zero 
	 *                      signifies a non-expiring entry.
	 */
	public ExpirableAuthorization(final ReadOnlyAuthorization authorization, 
		                      final long lifetime) {

		super(authorization.getIssuer(),
		      authorization.getSubject(),
		      authorization.getAudience(),
		      authorization.getScope(),
		      authorization.getData());

		this.lifetime = lifetime;
	}


	/**
	 * Gets the lifetime.
	 *
	 * @return The lifetime of the entry in seconds. Zero signifies a 
	 *         non-expiring entry.
	 */
	public long getLifetime() {

		return lifetime;
	}
}