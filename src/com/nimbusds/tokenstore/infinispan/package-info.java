/**
 * Infinispan - based token store implementation.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ ($version-date$)
 */
 package com.nimbusds.tokenstore.infinispan;