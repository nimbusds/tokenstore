/**
 * The base interfaces and classes for an OAuth 2.0 access and refresh token
 * store.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ ($version-date$)
 */
package com.nimbusds.tokenstore;