package com.nimbusds.tokenstore;


/**
 * Access and refresh token pair. This class is immutable.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-10)
 */
public final class TokenPair {


	/**
	 * Access token.
	 */
	private final String accessToken;


	/**
	 * Refresh token, {@code null} if not specified.
	 */
	private final String refreshToken;


	/**
	 * The token lifetime in seconds, zero signifies non-expiring tokens.
	 */
	private final long lifetime;


	/**
	 * Creates a new non-expiring access and refresh token pair.
	 *
	 * @param accessToken  The access token. Must not be {@code null}.
	 * @param refreshToken The refresh token. If none {@code null}.
	 */
	public TokenPair(final String accessToken, final String refreshToken) {

		this(accessToken, refreshToken, 0);
	}


	/**
	 * Creates a new expiring access and refresh token pair.
	 *
	 * @param accessToken  The access token. Must not be {@code null}.
	 * @param refreshToken The refresh token. If none {@code null}.
	 * @param lifetime     The lifetime of the tokens in seconds. Zero
	 *                     signifies a non-expiring token pair.
	 */
	public TokenPair(final String accessToken, final String refreshToken, final long lifetime) {

		if (accessToken == null)
			throw new IllegalArgumentException("The access token must not be null");

		this.accessToken = accessToken;

		this.refreshToken = refreshToken;

		this.lifetime = lifetime;
	}


	/**
	 * Gets the access token.
	 *
	 * @return The access token.
	 */
	public String getAccessToken() {

		return accessToken;
	}


	/**
	 * Gets the refresh token.
	 *
	 * @return The refresh token. If none {@code null}.
	 */
	public String getRefreshToken() {

		return refreshToken;
	}


	/**
	 * Gets the token lifetime.
	 *
	 * @return The token lifetime in seconds. Zero signifies non-expiring
	 *         tokens.
	 */
	public long getLifetime() {

		return lifetime;
	}


	@Override
	public String toString() {

		return "TokenPair [accessToken=" + accessToken + 
		       " refreshToken=" + refreshToken +
		       " lifetime=" + lifetime + "]";
	}
}