package com.nimbusds.tokenstore;


/**
 * Authorisation granted by an access token.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-11)
 */
public class Authorization  implements ReadOnlyAuthorization {


	/**
	 * The issuer, {@code null} if not specified.
	 */
	private String issuer;


	/**
	 * The subject, {@code null} if not specified.
	 */
	private String subject;


	/**
	 * The audience, {@code null} if not specified.
	 */
	private String audience;


	/**
	 * The scope, {@code null} if not specified.
	 */
	private String scope;


	/**
	 * Optional data serialisable to JSON, {@code null} if not specified.
	 */
	private Object data;


	/**
	 * Creates a new authorisation with no initial properties. Use the
	 * appropriate setter(s) for that.
	 */
	public Authorization() {

	}


	/**
	 * Creates a new authorisation with the specified properties.
	 *
	 * @param issuer   The authorisation issuer, {@code null} if not
	 *                 specified.
	 * @param subject  The subject, {@code code} if not specified.
	 * @param audience The authorisation audience, {@code null} if not
	 *                 specified.
	 * @param scope    The authorisation scope, {@code null} if not
	 *                 specified.
	 * @param data     Optional data, {@code null} if not specified. Should
	 *                 be serialisable to JSON.
	 */
	public Authorization(final String issuer, final String subject,
		             final String audience, final String scope, 
		             final Object data) {

		this.issuer = issuer;
		this.subject = subject;
		this.audience = audience;
		this.scope = scope;
		this.data = data;
	}


	@Override
	public String getIssuer() {

		return issuer;
	}


	/**
	 * Sets the issuer of this authorisation.
	 *
	 * @param issuer The issuer, {@code null} if not specified.
	 */
	public void setIssuer(final String issuer) {

		this.issuer = issuer;
	}


	@Override
	public String getSubject() {

		return subject;
	}


	/**
	 * Sets the subject of this authorisation.
	 *
	 * @param subject The subject, {@code null} if not specified.
	 */
	public void setSubject(final String subject) {

		this.subject = subject;
	}


	@Override
	public String getAudience() {

		return audience;
	}


	/**
	 * Sets the audience of this authorisation.
	 *
	 * @param audience The audience, {@code null} if not specified.
	 */
	public void setAudience(final String audience) {

		this.audience = audience;
	}


	@Override
	public String getScope() {

		return scope;
	}


	/**
	 * Sets the scope of this authorisation.
	 *
	 * @param scope The scope, {@code null} if not specified.
	 */
	public void setScope(final String scope) {

		this.scope = scope;
	}


	@Override
	public Object getData() {

		return data;
	}


	/**
	 * Sets the optional data of this authorisation.
	 *
	 * @param data The optional data, serialisable to JSON, {@code null} if
	 *             not specified.
	 */
	public void setData(final Object data) {

		this.data = data;
	}
}