package com.nimbusds.tokenstore;


/**
 * Read-only authorisation granted by an access token.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-11)
 */
public interface ReadOnlyAuthorization {


	/**
	 * Gets the issuer of this authorisation.
	 *
	 * @return The issuer, {@code null} if not specified.
	 */
	public String getIssuer();


	/**
	 * Gets the subject of this authorisation.
	 *
	 * @return The subject, {@code null} if not specified.
	 */
	public String getSubject();


	/**
	 * Gets the audience of this authorisation.
	 *
	 * @return The audience, {@code null} if not specified.
	 */
	public String getAudience();


	/**
	 * Gets the scope of this authorisation.
	 *
	 * @return The scope, {@code null} if not specified.
	 */
	public String getScope();


	/**
	 * Gets the optional data of this authorisation.
	 *
	 * @return The optional data, serialisable to JSON, {@code null} if not
	 *         specified.
	 */
	public Object getData();
}