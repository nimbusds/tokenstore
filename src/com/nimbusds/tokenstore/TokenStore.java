package com.nimbusds.tokenstore;


/**
 * Token store interface. Intended for storage of OAuth 2.0 access and refresh
 * tokens. Handles token generation and refreshing internally.
 *
 * <p>See The OAuth 2.0 Authorization Framework (RFC 6749) for details.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-11)
 */
public interface TokenStore {


	/**
	 * The default length of generated access and refresh tokens 
	 * (alphanumeric characters).
	 */
	public static final int DEFAULT_TOKEN_LENGTH = 32;


	/**
	 * Gets the length of the generated access and refresh tokens 
	 * (alphanumeric characters).
	 *
	 * @return The token length.
	 */
	public int getTokenLength();


	/**
	 * Sets the length of the generated access and refresh tokens
	 * (alphanumeric characters).
	 *
	 * @param length The token length. Must be a positive integer.
	 */
	public void setTokenLength(final int length);


	/**
	 * Adds a new non-expiring entry to the token store.
	 *
	 * @param authorization The authorisation record for the access token. 
	 *                      Must not be {@code null}.
	 *
	 * @return The generated token pair for the entry, with only an access
	 *         token specified. The token is not set to expire.
	 */
	public TokenPair addEntry(final ReadOnlyAuthorization authorization);


	/**
	 * Adds a new expiring entry to the token store.
	 *
	 * @param authorization The authorisation record for the access token.
	 *                      Must not be {@code null}.
	 * @param lifetime      The lifetime of the entry in seconds. Zero
	 *                      signifies a non-expiring entry.
	 *
	 * @return The generated token pair for the entry, with only an access
	 *         token specified. The token is set to expire according to the
	 *         specified lifetime.
	 */
	public TokenPair addEntry(final ReadOnlyAuthorization authorization, final long lifetime);


	/**
	 * Adds a new refreshable entry to the token store.
	 *
	 * @param authorization The authorisation record for the access token.
	 *                      Must not be {@code null}.
	 * @param lifetime      The lifetime of the entry in seconds. Zero
	 *                      signifies a non-expiring entry.
	 *
	 * @return The generated access and refresh token pair. The tokens are
	 *         set to expire according to the specified lifetime.
	 */
	public TokenPair addRefreshableEntry(final ReadOnlyAuthorization authorization, final long lifetime);


	/**
	 * Gets the authorisation record for the specified access token.
	 *
	 * @param accessToken The access token. Must not be {@code null}.
	 *
	 * @return The matching authorisation record, {@code null} if the 
	 *         access token is invalid or has expired.
	 */
	public ReadOnlyAuthorization getAuthorization(final String accessToken);


	/**
	 * Removes an entry from the token token store. If a refresh token is
	 * associated it will also be invalidated.
	 *
	 * @param accessToken The access token. Must not be {@code null}.
	 *
	 * @return The matching authorisation record, {@code null} if the 
	 *         access token is invalid or has expired.
	 */
	public ReadOnlyAuthorization removeEntry(final String accessToken);


	/**
	 * Refreshes an entry in the token store.
	 *
	 * @param refreshToken The refresh token. Must not be {@code null}.
	 *
	 * @return The new generated access and refresh token pair. The tokens
	 *         are set to expire according to the originally specified
	 *         lifetime. If the refresh token is invalid or has expired the
	 *         method will return {@code null}.
	 */
	public TokenPair refreshEntry(final String refreshToken);
}