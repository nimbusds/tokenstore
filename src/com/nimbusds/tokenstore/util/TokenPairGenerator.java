package com.nimbusds.tokenstore.util;


import org.apache.commons.lang3.RandomStringUtils;

import com.nimbusds.tokenstore.TokenPair;


/**
 * Token pair generator, using random alphanumeric strings.
 *
 * @author Vladimir Dzhuvinov
 * @version $version$ (2013-01-10)
 */
public class TokenPairGenerator {


	/**
	 * The configured token length.
	 */
	private final int length;


	/**
	 * Creates a new token pair generator.
	 *
	 * @param length The length of the tokens to generate. Must be a 
	 *               positive integer.
	 */
	public TokenPairGenerator(final int length) {

		if (length <= 0)
			throw new IllegalArgumentException("The token length must be positive");

		this.length = length;
	}


	/**
	 * Returns the length of the tokens to generate.
	 *
	 * @return The token length.
	 */
	public int tokenLength() {

		return length;
	}


	/**
	 * Generates a new token pair. 
	 *
	 * @param noRefreshToken If {@code true} no refresh token will be
	 *                       generated.
	 * @param lifetime       The token lifetime in seconds, zero signifies 
	 *                       non-expiring token(s).
	 *
	 * @return The generated token pair.
	 */
	public TokenPair generate(final boolean noRefreshToken, final long lifetime) {

		if (noRefreshToken)
			return new TokenPair(RandomStringUtils.randomAlphanumeric(length), 
				             null, 
				             lifetime);
		else
			return new TokenPair(RandomStringUtils.randomAlphanumeric(length),
				             RandomStringUtils.randomAlphanumeric(length),
				             lifetime);
	}
}